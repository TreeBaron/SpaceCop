﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceCop
{
    class Scenery : IDrawable
    {
        public Texture2D Texture { get; set; }
        public Vector2 Position { get; set; }
        public float Scale { get; set; }

        private Rectangle DrawBox { get; set; }

        public Scenery(Texture2D texture, Vector2 position, float scale)
        {
            Texture = texture;
            Position = position;
            Scale = scale;
            DrawBox = new Rectangle((int)Position.X,
                                    (int)Position.Y,
                                    (int)(Texture.Width * Scale),
                                    (int)(Texture.Height * Scale));
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, DrawBox, Color.White);
        }

        public void Update(GameTime gameTime)
        {
            // nothing
        }
    }
}
